public class StackApp {

    public static void main(String[] args) {
        // initialize names to array becase stack is not a valid input
        // type for enhanced for loops
        String[] inputArray = new String[3];
        inputArray[0] = "Apples";
        inputArray[1] = "Oranges";
        inputArray[2] = "Bannanas";
        Stack<String> s = new Stack<>();
        int i=0;
        // STEP 5: Enhanced for loop
        for (String element:inputArray){
            s.push(element);
            System.out.format("Push: %s\n", (s.peek()));
        }

        // STEP 6:peek at the first item
            System.out.println(""); // blank line for readability
        System.out.println("Peek: " + s.peek());
        System.out.println("The stack contains " + s.size() + " items\n");
// STEP 7: while loop for popping
        while (s.size()>0){
        System.out.println("Pop: " + s.pop());
        } // end while loop
    System.out.println("The stack contains " + s.size() + " items\n");
    
}
}
